from selenium import webdriver
from selenium.webdriver.firefox.options import Options


def get_driver(headless):
    firefox_profile = webdriver.FirefoxProfile()

    options = Options()
    options.headless = headless

    driver = webdriver.Firefox(options=options, firefox_profile=firefox_profile)
    return driver
