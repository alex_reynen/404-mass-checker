from gui import popup_msg


def about_popup():
    message = '''Author: Alex Reynen (co-op summer/fall 2019)
    Contact: alexreynen@gmail.com
    
    This 404 mass checker is made for use in checking a page,
    and the given links within that page.
    Documentation can be found in the word document also downloaded
    with this application.
    This current use is the GUI implementation, however, you may find
    the console implementation better for your use case as there are
    some options only available to that.
    Run with --help or -h to see options.'''

    title = 'About'

    return popup_msg(message, title)


if __name__ == '__main__':
    about_popup()
