import tkinter as tk


def popup_msg(msg, title):
    popup = tk.Tk()
    popup.wm_title(title)
    popup.resizable(False, False)
    label = tk.Label(popup, text=msg)
    label.config(width=60)
    label.grid(row=0)

    exit_button = tk.Button(popup, text='Close')
    exit_button.grid(row=1)
    return popup, exit_button


if __name__ == '__main__':
    popup_msg('Hello World', 'About')
