import os
import threading
import tkinter as tk
from tkinter import filedialog
import tkinter.scrolledtext as scrolledtext

from gui import file_runner
from gui import link_runner
from gui import about_popup
from gui import popup_msg

root = tk.Tk()


def console_print(elem):
    print(elem)


class Application(tk.Frame):
    def __init__(self, master=None, about=None, error=None):
        super().__init__(master)
        self.master = master
        self.about = about
        self.error = error
        self.view_console = False
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.path_text = tk.Text(self)
        self.path_text.insert('end', 'Hello, type a path or url.  Select a path to the right ->')
        self.path_text.grid(row=0, column=0, columnspan=2, padx=5, pady=5)
        self.path_text.config(width=126, height=0)
        self.path_text.bind('<Button-1>', self.clear_path_text)
        self.path_text.config(state='disabled')

        self.file_button = tk.Button(self)
        self.file_button['text'] = 'Select file'
        self.file_button['command'] = self.select_file
        self.file_button.grid(row=0, column=2, columnspan=3, padx=5, pady=5)

        self.about_button = tk.Button(self)
        self.about_button['text'] = 'About'
        self.about_button['command'] = self.trigger_about_popup
        self.about_button.grid(row=1, column=4)

        self.run_button = tk.Button(self, text='Run', fg='green')
        self.run_button.grid(row=1, columnspan=2)
        self.run_button.config(width=25)
        self.run_button['command'] = self.starter

        self.path_text.bind('<Return>', self.starter)

        self.view_checkbox = tk.Checkbutton(self)
        self.view_checkbox.grid(row=2, column=4, sticky='N')
        self.view_checkbox['command'] = self.change_scrollbar
        self.view_checkbox['text'] = 'View Console Log'

        self.conosle_log = scrolledtext.ScrolledText(self, relief='ridge')
        self.conosle_log.grid(row=2, column=0, rowspan=2)
        self.conosle_log.config(width=125)
        self.conosle_log.grid_forget()

        self.clear_button = tk.Button(self)
        self.clear_button['text'] = 'Clear Log'
        self.clear_button['command'] = self.clear_scrollbar
        self.clear_button.grid(row=3, column=4, sticky='N')
        self.clear_button.grid_forget()

    def clear_path_text(self, event=None):
        if self.path_text['state'] == 'disabled':
            self.path_text.config(state='normal')
            self.path_text.delete('1.0', tk.END)

    def clear_scrollbar(self):
        if self.conosle_log['state'] == 'normal':
            self.conosle_log.delete(1.0, tk.END)
        else:
            self.conosle_log['state'] = 'normal'
            self.conosle_log.delete(1.0, tk.END)
            self.conosle_log['state'] = 'disabled'

    def gui_print(self, elem):
        self.conosle_log['state'] = 'normal'
        self.conosle_log.insert('end', str(elem) + '\n')
        self.conosle_log.see(tk.END)
        # self.conosle_log['state'] = 'disabled'

    def change_scrollbar(self):
        if self.view_console:
            self.view_console = False
            self.conosle_log.grid_forget()
            self.clear_button.grid_forget()
        else:
            self.view_console = True
            self.conosle_log.config(state='disabled')
            self.clear_button.grid(row=3, column=4, sticky='N')
            self.conosle_log.grid(row=2, column=0, rowspan=150)

    def select_file(self):
        root.filename = filedialog.askopenfilename(initialdir='/', title='Select file', filetypes=(
            ('Excel Document', '*.xlsx *.xls'),))
        self.clear_path_text()
        self.update_for_run(root.filename)

    def update_for_run(self, text):
        if text != '':
            self.path_text.delete(1.0, tk.END)
            self.path_text.insert('end', text)

    def trigger_about_popup(self):
        self.about, close_button = about_popup()
        self.about_button.config(state='disabled')
        self.about.protocol('WM_DELETE_WINDOW', self.restore_about_button)
        close_button.config(command=self.restore_about_button)
        self.about.mainloop()

    def restore_about_button(self):
        self.about.destroy()
        self.about_button.config(state='normal')

    def starter(self, event=None):
        def runner(type: int):
            self.run_button['state'] = 'disabled'
            self.file_button['state'] = 'disabled'
            file_runner(self.path_text.get('1.0', tk.END).strip(), self.gui_print, True) if type == 0 else link_runner(
                self.path_text.get('1.0', tk.END).strip(), self.gui_print)
            self.run_button['state'] = 'normal'
            self.file_button['state'] = 'normal'
            # Big broke atm
            # self.trigger_about_popup()
            # self.trigger_error_popup(result, 'Finished')

        text_text = self.path_text.get('1.0', tk.END).strip()
        valid, runner_type = analyze_text(text_text)
        if not valid:
            self.trigger_error_popup('''The URL or file path is invalid.
Please ensure you are using the whole URL
(https:// included) or using the full path.''', 'Invalid Entry')
            return 'break'

        p = threading.Thread(target=runner, args=(runner_type,))
        p.start()
        return 'break'

    def trigger_error_popup(self, message, title):
        self.error, close_button = popup_msg(message, title)
        self.run_button.config(state='disabled')
        self.error.protocol('WM_DELETE_WINDOW', self.restore_run_button)
        close_button.config(command=self.restore_run_button)
        self.error.mainloop()

    def restore_run_button(self):
        self.error.destroy()
        self.run_button.config(state='normal')


def analyze_text(text):
    if is_valid_path(text):
        return True, 0
    elif is_valid_link(text):
        return True, 1
    return False, None


def is_valid_path(text_text):
    return (os.path.isfile(text_text)) and ('xlxs' in text_text or 'xls' in text_text)


def is_valid_link(text_text):
    if 'http' not in text_text:
        return False
    from console.link_checker import checker
    return checker(text_text, [], str)[0]


def run():
    root.title('404 Mass Checker')
    root.resizable(False, False)
    app = Application(master=root)
    app.mainloop()

# if __name__ == '__main__':
#     print(is_valid_link('https://www.servicenow.com/fake-404.html'))
