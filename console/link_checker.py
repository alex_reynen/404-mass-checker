import requests


def checker(site: str, list: list, printer):
    # print(f'\t - Checking: {site}')
    if site in list:
        return True, None

    if 'http' not in site or 'mailto' in site:
        return True, None

    try:
        r = requests.get(site, headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:68.0) Gecko/20100101 Firefox/68.0'})
        response = r.status_code
    except requests.exceptions.ConnectionError:
        return False, 'Connection refused'

    if response < 400 or response == 999:
        list.append(site)
        return True, None

    return False, response


if __name__ == '__main__':
    from console.main import normal_print

    for i in ['mailto:?subject=Workflow Quarterly&body=https://workflow.servicenow.com/quarterly/issue/1/']:
        print(checker(i, [], normal_print))
