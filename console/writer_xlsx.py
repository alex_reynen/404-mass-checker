# Write xlsx
import os

from openpyxl import load_workbook
from openpyxl.styles import Alignment


def write(file_path, work_list, sheet_name):
    # input()
    wb = load_workbook(file_path)
    ws = wb[sheet_name]
    # ws['A1'] = 'A1'
    for index, work in enumerate(work_list):
        ws[f'B{index + 1}'].alignment = Alignment(wrapText=True)
        ws[f'B{index + 1}'] = work.print_problems()

    # wb.save(file_path + '.out' + os.path.splitext(file_path)[-1])

    # wb = load_workbook(file_path)
    # ws = wb['Sheet1']
    # ws['A1'] = 'A1'
    # wb.save(file_path)
    # wb.save(file_path)
    wb.save(os.path.dirname(file_path) + f'/out.xlsx')



if __name__ == '__main__':
    write('/Users/alexander.reynen/Desktop/404 Mass Checker/test/Book3.xlsx', [])
