from console.link_checker import checker

import threading


# Subprocess running
def multi_link_checking(url_list, good_list, i, printer):
    jobs = []
    for url in url_list:
        p = threading.Thread(target=checker_runner, args=(url, good_list, i, printer))
        jobs.append(p)
        p.start()
    for j in jobs:
        j.join()
    # printer(i.problems)
    return good_list


def checker_runner(site, list, i, printer):
    result, response = checker(site, list, printer)
    if not result:
        i.problems.append((site, response))
        printer(f'\t - Error: {site}, {response}')
