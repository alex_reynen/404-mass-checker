# Read xlsx
import xlrd

from console.job import Job


def read(file, sheet_num=0):
    workbook = xlrd.open_workbook(file)

    sheet_name = workbook.sheet_names()[sheet_num]

    sheet = workbook.sheet_by_index(sheet_num)
    url_list = []
    for row in range(0, sheet.nrows):
        url_list.append(Job(sheet.cell_value(row, 0)))
    return url_list, sheet_name


if __name__ == '__main__':
    path = input('What is the path?: ')

    url_list = read(path)

    for i in url_list: print(i)
