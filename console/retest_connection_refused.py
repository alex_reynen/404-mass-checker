import threading

from console import checker


def multi_retest(job_list, good_list, printer):
    work_list = []
    for job in job_list:
        if len(job.problems) > 0:
            printer(f'Retesting bad links from {job.site}.')
            p = threading.Thread(target=link_check, args=(good_list, job, printer))
            work_list.append(p)
            p.start()
    for j in work_list:
        j.join()


def retest(job_list, good_list, printer):
    for job in job_list:
        if len(job.problems) > 0:
            printer(f'Retesting bad links from {job.site}.')
            link_check(good_list, job, printer)


def link_check(good_list, job, printer):
    for problem in job.problems:
        if (checker(problem[0], good_list, printer))[0]:
            job.problems.remove(problem)
            printer(f' - {problem[0]} found to be OK.')
        else:
            printer(f' - {problem[0]} found to still be problematic.')


if __name__ == '__main__':
    from console.main import normal_print
    from console.job import Job

    i = [Job('https://www.servicenow.com/fake-link.html',
             [('https://www.servicenow.com/fake-link.html', 'Connection refused')])]
    print(retest(i, [], normal_print))
