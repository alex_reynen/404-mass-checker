import argparse
import os

import selenium
import xlrd
from shutil import move
from pathlib import Path

from .retest_connection_refused import retest, multi_retest
from console import multi_link_checking
from console import read
from console import write
from console.job import Job
from common import get_driver

REFRESH_NUMBER = 200

def normal_print(elem):
    print(elem)


def file_runner(path, printer=normal_print, headless=True):

    good_list = []
    for i in range(0, len(xlrd.open_workbook(path).sheet_names())):
        driver = get_driver(headless)

        try:
            job_list, sheet_name = read(path, i)
        except FileNotFoundError:
            printer('File does not exist, please double check the path.')
            return

        for index, job in enumerate(job_list):
            if index is not 0 and index % 200 is 0:
                driver.quit()
                driver = get_driver(headless)
            printer(f'Checking site {index + 1} / {len(job_list)} in {sheet_name}')
            printer(f'Firefox Navigating to {job.site}')
            try:
                driver.get(job.site)
                elems = list(dict.fromkeys(driver.find_elements_by_xpath('//a[@href]')))
                printer('Successfully Navigated')
            except selenium.common.exceptions.WebDriverException as e:
                printer('Unable to navigate, skipping this page')
                job.problems.append(f'Unable to check page as error encountered:\n{e}')
                elems = []

            site_list = []
            for elem in elems:
                try:
                    href = elem.get_attribute('href')
                    if href not in good_list:
                        site_list.append(elem.get_attribute('href'))
                except selenium.common.exceptions.StaleElementReferenceException:
                    pass

            site_list = list(set(site_list))
            printer(f' - Testing {len(site_list)} Links...')
            good_list = multi_link_checking(site_list, good_list, job, printer)
            printer(f' - {len(job.problems)} Errors Found in {job.site}')

        # retest(job_list, good_list, printer)
        multi_retest(job_list, good_list, printer)

        write(path if i is 0 else 'test/out.xlsx', job_list, sheet_name)

        num_problems = 0
        for job in job_list:
            num_problems += job.get_num_problems()

        result = f'{num_problems} found.\n\t - Out Excel Updated, Please Check.'
        printer(result)

        driver.quit()


def link_runner(url: str, printer=normal_print, headless=True):
    driver = get_driver(headless)

    good_list = []

    job = Job(url)

    printer(f'Firefox Navigating to {job.site}')
    try:
        driver.get(job.site)
    except selenium.common.exceptions.InvalidArgumentException:
        printer(f"Malformed URL: {job.site}:")
        return

    printer('Successfully Navigated')
    elems = list(dict.fromkeys(driver.find_elements_by_xpath('//a[@href]')))

    site_list = []
    [site_list.append(elem.get_attribute('href')) for elem in elems]
    printer(f' - Testing {len(site_list)} Links...')
    multi_link_checking(site_list, good_list, job, printer)
    printer(f' - {len(job.problems)} Error{"s" if len(job.problems) > 0 else ""} Found in {job.site}')

    driver.quit()

    retest([job], good_list, printer)

    result = f'Issues found: {len(job.problems)}\n\t - {job.print_problems()}'
    printer(result)
    return result


def main(path, url, headless):
    if path is None and url is not None:
        link_runner(url, headless=headless)
    elif url is None and path is not None:
        file_runner(path, headless=headless)
    elif path and url is not None:
        print('URL and Path both set, please only use one')
    else:
        print('Please set either a path or url.  "-h" or "--help" for help')
