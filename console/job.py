class Job:

    def __init__(self, site, problems=None):
        if problems is None:
            problems = []
        self.site = site
        self.problems = problems

    def __str__(self):
        return f'{self.site} | {self.problems}'

    def print_problems(self):
        if len(self.problems) != 0:
            str_const = ''
            for i in self.problems:
                str_const += f'{i[0]} | {i[1]}\n'
            return str_const.rstrip()
        else:
            return 'Pass'

    def get_num_problems(self):
        return len(self.problems)
