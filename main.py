#!/usr/bin/env python3

import argparse

from gui.main_gui import run as gui_main
from console.main import main as console_main

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-console', help='Use if console', default=False, action='store_true')
    parser.add_argument('--url', help='Sets the URL (console only)', type=str)
    parser.add_argument('--path', help='Sets the path for an excel document (console only)', type=str)
    parser.add_argument('--not_headless', help='Include to see the browser (console only)', default=False, action='store_true')

    args = parser.parse_args()
    console = args.console
    url = args.url
    path = args.path
    headless = args.not_headless

    if console:
        console_main(path, url, not headless)
    else:
        gui_main()
